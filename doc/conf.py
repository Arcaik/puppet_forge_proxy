# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

# General configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.coverage',
]

templates_path = ['_templates']

source_suffix = '.rst'

master_doc = 'index'

language = 'en'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = 'sphinx'


# Options for HTML output

html_theme = 'alabaster'

html_theme_options = {
    'page_width': '90%',
    'sidebar_width': '20%',
}

html_static_path = ['_static']


# Options for autodoc extension

autodoc_member_order = 'bysource'
autoclass_cotent = 'both'


# Options for intersphinx extension

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None)
}
