Puppet Forge Proxy
================

`puppet_forge_proxy` is a Python application written with Flask that proxifies
and caches requests to the [Puppet forge API version
3](http://forgeapi.puppetlabs.com/).


Documentation
---------------------

See: https://arcaik.gitlab.io/puppet_forge_proxy/


Features
------------

`puppet_forge_proxy` implements only three of the API endpoints:

* `/v3/modules/{user}-{module}`
* `/v3/releases/{user}-{module}-{version}`
* `/v3/files/{filename}`


Todo
-------

* Support for “local” modules uploading (with authentication).


Similar projects
---------------------

* [Puppet Forge Server](https://github.com/unibet/puppet-forge-server) (ruby).
* [django-forge](https://github.com/jbronn/django-forge) (Python, Django).
