#!/usr/bin/python3
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import puppet_forge_proxy

from setuptools import setup
from sphinx.setup_command import BuildDoc


version = '.'.join(puppet_forge_proxy.__version__.split('.')[0:2])
release = puppet_forge_proxy.__version__

setup(
    name=puppet_forge_proxy.__title__,
    description=puppet_forge_proxy.__description__,
    author=puppet_forge_proxy.__author__,
    license=puppet_forge_proxy.__license__,
    version=release,
    url='https://gitlab.com/Arcaik/puppet_forge_proxy',
    cmdclass={
        'build_sphinx': BuildDoc,
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', 'puppet_forge_proxy'),
            'version': ('setup.py', version),
            'release': ('setup.py', release),
        },
    },
)
