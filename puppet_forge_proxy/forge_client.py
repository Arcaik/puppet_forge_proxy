# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import requests

from puppet_forge_proxy import __version__


FORGE_URL = 'https://forgeapi.puppetlabs.com'
DEFAULT_HEADERS = {
    'User-Agent': '{}/{} {}'.format(
        'puppet_forge_api', __version__, requests.utils.default_user_agent()
    )
}


def get(endpoint: str, headers: dict = {}):
    headers.update(DEFAULT_HEADERS)

    r = requests.get(FORGE_URL + endpoint, headers=headers)
    r.raise_for_status()

    return r


def get_module(user: str, module: str):
    try:
        r = get('/v3/modules/{}-{}'.format(user, module))
    except requests.HTTPError as e:
        return e.response.json(), e.response.status_code
    except Exception as e:
        return {'message': str(e)}, 500

    return (r.json(), r.status_code)


def get_release(user: str, module: str, version: str):
    try:
        r = get('/v3/releases/{}-{}-{}'.format(user, module, version))
    except requests.HTTPError as e:
        return e.response.json(), e.response.status_code
    except Exception as e:
        return {'message': str(e)}, 500

    return (r.json(), r.status_code)


def get_release_file(filename: str):
    try:
        r = get('/v3/files/{}'.format(filename))
    except requests.HTTPError as e:
        return e.response.json(), e.response.status_code
    except Exception as e:
        return {'message': str(e)}, 500

    return (r.content, r.status_code)
