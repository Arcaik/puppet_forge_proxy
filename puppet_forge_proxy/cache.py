# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import os
import json
import hashlib
import time
import functools

from flask import safe_join

from puppet_forge_proxy.app import app


CACHE_DIRECTORY = os.path.join(
    app.root_path, app.config.get('RELEASE_FILE_DIRECTORY', 'files')
)


def _cache_writer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            os.mkdir(CACHE_DIRECTORY, 0o755)
        except FileExistsError:
            pass
        except OSError as e:
            msg = 'Unable to create cache directory: {}'
            app.logger.error(msg.format(e.strerror))
            return None

        try:
            return func(*args, **kwargs)
        except OSError as e:
            msg = 'Unable to save file to cache: {}'
            app.logger.error(msg.format(e))

    return wrapper


def _cache_reader(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except OSError as e:
            msg = 'Unable to access file from cache: {}'
            app.logger.error(msg.format(e))

    return wrapper


def validate_filename(filename):
    filename = safe_join('', filename)

    seperators = list(
        sep for sep in [os.path.sep, os.path.altsep] if sep is not None
    )

    if any(sep in filename for sep in seperators):
        raise ValueError

    return filename


def checksum(filename_or_bytes):
    if isinstance(bytes, filename_or_bytes):
        hashlib.sha1(filename_or_bytes).hexdigest()

    if isinstance(str, filename_or_bytes):
        with open(filename_or_bytes, 'rb') as f:
            return hashlib.sha1(f.read()).hexdigest()

    msg = 'Was excpecting a filname or byte data, got {}'
    raise TypeError(msg.format(type(filename_or_bytes)))


@_cache_reader
def get_module(user: str, module: str):
    file_path = os.path.join(
        CACHE_DIRECTORY, '{}-{}.json'.format(user, module)
    )

    if not os.path.exists(file_path):
        return

    cache_time = app.config.get('MODULE_FILE_CACHE_TIME', 300)

    if time.time() - os.stat(file_path).st_mtime > cache_time:
        app.logger.info('Removing old module file: {}'.format(file_path))
        os.remove(file_path)
        return

    try:
        with open(file_path, 'r') as f:
            return json.load(f)
    except json.JSONDecodeError:
        app.logger.info(
            'Removing corrupted release file: {}'.format(file_path)
        )
        os.remove(file_path)
    except OSError as e:
        msg = 'Unable to access file from cache: {}'
        app.logger.error(msg.format(e.strerror))


@_cache_writer
def save_module(user: str, module: str, content: dict):
    file_path = os.path.join(
        CACHE_DIRECTORY, '{}-{}.json'.format(user, module)
    )

    if os.path.exists(file_path):
        return

    try:
        with open(file_path, 'w') as f:
            json.dump(content, f)
    except OSError as e:
        msg = 'Unable to access file from cache: {}'
        app.logger.error(msg.format(e.strerror))

    return file_path


@_cache_reader
def get_release(user: str, module: str, version: str):
    file_path = os.path.join(
        CACHE_DIRECTORY, '{}-{}-{}.json'.format(user, module, version)
    )

    if not os.path.exists(file_path):
        return

    try:
        with open(file_path, 'r') as f:
            return json.load(f)
    except json.JSONDecodeError:
        app.logger.info(
            'Removing corrupted release file: {}'.format(file_path)
        )
        os.remove(file_path)
    except OSError as e:
        msg = 'Unable to access file from cache: {}'
        app.logger.error(msg.format(e.strerror))


@_cache_writer
def save_release(user: str, module: str, version: str, content: dict):
    file_path = os.path.join(
        CACHE_DIRECTORY, '{}-{}-{}.json'.format(user, module, version)
    )

    if os.path.exists(file_path):
        return

    try:
        with open(file_path, 'w') as f:
            json.dump(content, f)
    except OSError as e:
        msg = 'Unable to access file from cache: {}'
        app.logger.error(msg.format(e.strerror))

    return file_path


@_cache_reader
def get_release_file(filename: str):
    file_path = os.path.join(CACHE_DIRECTORY, filename)

    if not os.path.exists(file_path):
        return

    try:
        with open(file_path, 'r') as f:
            return f
    except OSError as e:
        msg = 'Unable to access file from cache: {}'
        app.logger.error(msg.format(e))
        return None


@_cache_writer
def save_release_file(filename: str, content: dict):
    file_path = os.path.join(CACHE_DIRECTORY, filename)

    if os.path.exists(file_path) and checksum(file_path) == checksum(content):
        return

    with open(file_path, 'wb') as f:
        f.write(content)

    return file_path
