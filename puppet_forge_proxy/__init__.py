# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

__title__ = 'puppet_forge_proxy'
__description__ = 'A Puppet forge API proxy made with Flask.'
__author__ = 'Johan Fleury (jfleury@arcaik.net)'
__version__ = '0.2.0'
__license__ = 'AGPLv3+'
