# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Johan Fleury
#
# ------------------------------ License ------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import tempfile

from flask import send_file
from flask_restplus import Api, Resource

from puppet_forge_proxy import __version__, __description__
from puppet_forge_proxy import cache
from puppet_forge_proxy import forge_client


api = Api(
    title='Puppet Forge Proxy API', description=__description__,
    version=__version__, default='API', default_label=None, ordered=True
)


@api.route('/v3/modules/<string:user>-<string:module>')
class Module(Resource):
    @api.response(200, 'Module found')
    @api.response(404, 'Module not found')
    def get(self, user: str, module: str):
        data = cache.get_module(user, module)

        if data:
            return (data, 200)

        data, status_code = forge_client.get_module(user, module)

        if status_code == 200:
            cache.save_module(user, module, data)

        return (data, status_code)


@api.route('/v3/releases/<string:user>-<string:module>-<string:version>')
class Release(Resource):
    @api.response(200, 'Release found')
    @api.response(404, 'Release not found')
    def get(self, user: str, module: str, version: str):
        data = cache.get_release(user, module, version)

        if data:
            return (data, 200)

        data, status_code = forge_client.get_release(user, module, version)

        if status_code == 200:
            cache.save_release(user, module, version, data)

        return (data, status_code)


@api.route('/v3/files/<string:filename>')
@api.produces('application/gzip')
@api.produces('application/x-tar')
class ReleaseFile(Resource):
    @api.response(200, 'File found')
    @api.response(404, 'File not found')
    def get(self, filename: str):
        try:
            filename = cache.validate_filename(filename)
        except ValueError:
            api.abort(400, 'Incorect filename')

        release_file = cache.get_release_file(filename)

        if release_file:
            return send_file(release_file.name)

        data, status_code = forge_client.get_release_file(filename)

        if status_code == 200:
            release_file = cache.save_release_file(filename, data)
        else:
            return (data, status_code)

        # Cache might not be accessible but we still want flask.send_file() to
        # work.
        if not release_file:
            release_file = tempfile.SpooledTemporaryFile(max_size=500*1024)
            release_file.write(data)
            release_file.seek(0)

        return send_file(release_file, attachment_filename=filename)
